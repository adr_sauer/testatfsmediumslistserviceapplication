# README #

### Testing application for AtfsMediumsListService ###

* Quick summary

This is a Windows Forms application allowing tests of a Web Service providing mediums data to Automatic Tank Filling System.
The purpose of this code repository is to present my skills in C# and .NET Framework programming.

* Version 1.0.*

### Technologies used ###

* WCF - Web service providing mediums data. Files: MainForm.cs, AtfsMediumsListService.cs, IAtfsMediumsListService.cs, Medium.cs

* Windows Forms - DataGridView allowing for input of testing data. Input to DataGridView is validated. Files: MainForm.cs, AtfsMediumsListFormDataSource.cs

### Contact ###

* Adrian Sauer

My LinkedIn profile: https://pl.linkedin.com/in/adrian-sauer-692b86138