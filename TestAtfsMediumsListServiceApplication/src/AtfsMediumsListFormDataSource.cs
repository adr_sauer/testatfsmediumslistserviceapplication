﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib;
using System.Windows.Forms;

namespace TestAtfsMediumsListServiceApplication.src
{
    /// <summary>
    /// Represents mediums data source based on input from DataGridView.
    /// </summary>
    public class AtfsMediumsListFormDataSource : IAtfsMediumsListDataSource
    {
        private DataGridView _dataGridView;

        /// <summary>
        /// Initializes a new instance of the AtfsMediumsListFormDataSource class using the specified DataGridView reference.
        /// </summary>
        /// <param name="dataGridView">DataGridView providing mediums data.</param>
        public AtfsMediumsListFormDataSource(DataGridView dataGridView)
        {
            _dataGridView = dataGridView;
        }

        /// <summary>
        /// Gets a table of available mediums based on DataGridView input.
        /// </summary>
        /// <returns>Table of mediums. IDs are guaranteed to be unique.</returns>
        public Medium[] GetAtfsMediums()
        {            
            var list = new Dictionary<int, Medium>();

            foreach (DataGridViewRow row in _dataGridView.Rows)
            {
                //Add only rows containing valid data.
                DataGridViewCell idCell = row.Cells[0];
                DataGridViewCell mediumNameCell = row.Cells[1];

                string idString = idCell.FormattedValue.ToString();
                string mediumNameString = mediumNameCell.FormattedValue.ToString();
                int id = 0;
                if ((mediumNameString != null) && (Int32.TryParse(idString, out id)))
                {                    
                    try
                    {
                        //Add only mediums with unique ID.
                        list.Add(id, new Medium(id, mediumNameString));
                    }
                    catch (ArgumentException)
                    { }
                }
            }
            return list.Values.ToArray();
        }
    }
}
