﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib
{
    /// <summary>
    /// Represents an implementation of AtfsMediumsList service. Class is thread-safe.
    /// </summary>
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,Namespace=AtfsMediumsListServiceConstants._atfsMediumsServiceNamespace)]
    public class AtfsMediumsListService : IAtfsMediumsListService
    {
        private IAtfsMediumsListDataSource _dataSource=null;
        private object _lockObject;

        /// <summary>
        /// Gets or sets the data source which provides mediums data.
        /// </summary>
        public IAtfsMediumsListDataSource DataSource
        {
            get
            {
                lock (_lockObject)
                {
                    return _dataSource;
                }
            }
            set
            {
                lock (_lockObject)
                {
                    if (value == null)
                        throw new ArgumentNullException();
                    _dataSource = value;
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the AtfsMediumsListService class using the specified data source.
        /// </summary>
        /// <param name="dataSource">Mediums data source.</param>
        public AtfsMediumsListService(IAtfsMediumsListDataSource dataSource)
        {
            _lockObject = new object();
            _dataSource = dataSource;
        }

        /// <summary>
        /// Gets a table of available mediums.
        /// </summary>
        /// <returns>Table of mediums. IDs are guaranteed to be unique.</returns>
        public Medium[] GetAtfsMediums()
        {
            lock (_lockObject)
            {
                if(_dataSource==null)
                    throw new ArgumentNullException("_dataSource","Data source not initialized.");
                return _dataSource.GetAtfsMediums();
            }            
        }
    }
}
