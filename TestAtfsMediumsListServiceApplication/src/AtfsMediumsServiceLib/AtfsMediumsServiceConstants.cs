﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib
{
    /// <summary>
    /// Provides constants common to AtfsMediumsListService.
    /// </summary>
    static class AtfsMediumsListServiceConstants
    {
        /// <summary>
        /// XML namespace for AtfsMediumsListService.
        /// </summary>
        public const string _atfsMediumsServiceNamespace = "http://atetech.pl/atfs/";
    }
}
