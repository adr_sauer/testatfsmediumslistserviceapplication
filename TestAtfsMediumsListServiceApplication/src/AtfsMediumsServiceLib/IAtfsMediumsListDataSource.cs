﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib
{
    /// <summary>
    /// Represents interface for mediums data source.
    /// </summary>
    public interface IAtfsMediumsListDataSource
    {
        /// <summary>
        /// Gets a table of available mediums.
        /// </summary>
        /// <returns>Table of mediums. IDs are guaranteed to be unique.</returns>
        Medium[] GetAtfsMediums();
    }
}
