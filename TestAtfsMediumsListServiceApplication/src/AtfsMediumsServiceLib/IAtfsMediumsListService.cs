﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib
{

    /// <summary>
    /// Represents service contract for AtfsMediumsListService
    /// </summary>
    [ServiceContract(Namespace = AtfsMediumsListServiceConstants._atfsMediumsServiceNamespace)]
    public interface IAtfsMediumsListService
    {
        /// <summary>
        /// Gets a table of available mediums.
        /// </summary>
        /// <returns>Table of mediums. IDs are guaranteed to be unique.</returns>
        [OperationContract]
        Medium[] GetAtfsMediums();
    }
}
