﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib
{
    /// <summary>
    /// Represents medium data.
    /// </summary>
    [DataContract]
    public class Medium
    {
        private const int _maxNameLength=20;
        

        private string _name=String.Empty;
        private int _id=0;

        /// <summary>
        /// Maximum length of medium name equal to 20.
        /// </summary>
        static public int MaxNameLength
        {

            get { return _maxNameLength; }
        }

        /// <summary>
        /// Initializes a new instance of the Medium class.
        /// </summary>
        public Medium()
        {
        }

        /// <summary>
        ///  Initializes a new instance of the Medium class using the specified id and name.
        /// </summary>
        /// <param name="id">Medium ID. Must be a positive integer.</param>
        /// <param name="name">Medium name. Must not be null and up to 20 characters long.</param>
        /// <exception cref="ArgumentException">ID not a positive integer, medium name null or longer than 20 characters.</exception>
        public Medium(int id,string name)
        {
            if (id < 0)
                throw new ArgumentException("Medium ID must be a positive integer.");
            if (name.Length > _maxNameLength)
                throw new ArgumentException("Maximum medium name length is 20");
            if (name == null)
                throw new ArgumentException("Medium name must not be null.");
            _id = id;
            _name = name;
        }
        
        /// <summary>
        /// Gets or sets medium name. Must not be null and up to 20 characters long.
        /// </summary>
        /// <exception cref="ArgumentException">Medium name null or longer than 20 characters.</exception>
        [DataMember]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Medium name must not be null.")]
        [StringLength(_maxNameLength, ErrorMessage = "Maximum medium name length is 20")]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                if (value.Length > _maxNameLength)
                    throw new ArgumentException("Maximum medium name length is 20");
                if (value == null)
                    throw new ArgumentException("Medium name must not be null.");
                _name = value;
            }
        }

        /// <summary>
        /// Gets or sets medium ID. Must be a positive integer.
        /// </summary>
        /// <exception cref="ArgumentException">ID not a positive integer.</exception>
        [DataMember]
        [Required(ErrorMessage = "Medium id must not be null.")]
        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                if (value < 0)
                    throw new ArgumentException("Medium ID must be a positive integer.");
                _id = value;
            }
        }
    }
}
