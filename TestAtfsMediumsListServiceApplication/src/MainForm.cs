﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib;
using System.ServiceModel;

namespace TestAtfsMediumsListServiceApplication.src
{
    /// <summary>
    /// Represents the main form of the application.
    /// </summary>
    public partial class MainForm : Form
    {
        private ServiceHost _serviceHost;
        private AtfsMediumsListService _service;
        private AtfsMediumsListFormDataSource _dataSource;

        /// <summary>
        /// Initializes a new instance of the MainForm class.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            _dataSource = new AtfsMediumsListFormDataSource(dataGridView1);
            _service = new AtfsMediumsListService(_dataSource);
            _serviceHost = new ServiceHost(_service);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _serviceHost.Close();
            e.Cancel = false;
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            _serviceHost.Open();
        }        

        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            // Clear the row error in case the user presses ESC.   
            dataGridView1.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            switch (e.ColumnIndex)
            {
                case 0:
                    ValidateIdCell(sender, e);
                    break;
                case 1:
                    ValidateNameCell(sender, e);
                    break;
                default:
                    throw new ArgumentException("Wrong column index");
            }
        }

        private void ValidateNameCell(object sender, DataGridViewCellValidatingEventArgs e)
        {
            string name = e.FormattedValue.ToString();
            if (name == null)
            {
                //Check if Name cell value is not null.
                dataGridView1.Rows[e.RowIndex].ErrorText = "Medium name must not be null.";
                e.Cancel = true;
            }
            else if (name.Length > Medium.MaxNameLength)
            {
                //Check if Name cell value is shorter than or equal to maximum name length.
                dataGridView1.Rows[e.RowIndex].ErrorText = String.Format( "Medium name must have at most {0} characters.", Medium.MaxNameLength);
                e.Cancel = true;
            }
        }

        private void ValidateIdCell(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int id=0;
            if (!Int32.TryParse(e.FormattedValue.ToString(), out id))
            {
                //Check if ID cell value parses to integer.
                dataGridView1.Rows[e.RowIndex].ErrorText = "ID must be a number.";
                e.Cancel = true;
            }
            else if (id < 0)
            {
                //Check if ID cell value is a positive number.
                dataGridView1.Rows[e.RowIndex].ErrorText = "ID must be a positive number.";
                e.Cancel = true;
            }
            else
            {
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    //Check every row, except the one being validated, if there is another one with the same ID.
                    if ((row.Cells[e.ColumnIndex].FormattedValue.ToString() == e.FormattedValue.ToString()) &&
                        e.RowIndex != dataGridView1.Rows.IndexOf(row))
                    {
                        dataGridView1.Rows[e.RowIndex].ErrorText = "ID must be unique.";
                        e.Cancel = true;
                    }
                }
            }
        }
    }
}
