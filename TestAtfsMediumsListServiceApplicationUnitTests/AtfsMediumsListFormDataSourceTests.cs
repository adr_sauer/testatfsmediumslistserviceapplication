﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestAtfsMediumsListServiceApplication.src;
using TestAtfsMediumsListServiceApplication.src.AtfsMediumsServiceLib;

namespace TestAtfsMediumsListServiceApplicationUnitTests
{
    [TestClass]
    public class AtfsMediumsListFormDataSourceTests
    {


        [TestMethod]
        public void GetAtfsMediumsTestValidData()
        {
            Medium[] testData = GenerateValidMediums(5, 5);

            DataGridView dataGridView = PopulateDataGridView(testData);

            var mediumsDataSource = new AtfsMediumsListFormDataSource(dataGridView);

            Medium[] resultData = mediumsDataSource.GetAtfsMediums();

            Assert.AreEqual(resultData.Length, testData.Length);

        }

        [TestMethod]
        public void GetAtfsMediumsTestNonUniqueIds()
        {
            Medium[] testData = GenerateValidMediums(5, 6);
            testData[5] = new Medium(1, "Name1");

            DataGridView dataGridView = PopulateDataGridView(testData);

            var mediumsDataSource = new AtfsMediumsListFormDataSource(dataGridView);

            Medium[] resultData = mediumsDataSource.GetAtfsMediums();
            Assert.AreEqual(resultData.Length, testData.Length-1);
        }



        private DataGridView PopulateDataGridView(Medium[] mediums)
        {
            var dataGridView=new DataGridView();
            var mediumNameColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            var idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();

            mediumNameColumn.HeaderText = "Name";
            mediumNameColumn.MaxInputLength = 20;
            mediumNameColumn.Name = "MediumName";
            mediumNameColumn.Width = 150;

            idColumn.HeaderText = "ID";
            idColumn.Name = "Id";

            dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] { idColumn, mediumNameColumn });

            for (int i = 0; i < mediums.Length; i++)            
                dataGridView.Rows.Add(mediums[i].Id.ToString(), mediums[i].Name.ToString());

            return dataGridView;
        }

        private Medium[] GenerateValidMediums(int count, int arrayLength)
        {
            if (count > arrayLength)
                throw new ArgumentException("Mediums count cannot be higher than array length.");

            Medium[] validMediums = new Medium[arrayLength];
            for (int i = 0; i < count; i++)
            {
                validMediums[i] = new Medium(i, "Name" + i.ToString());
            }
            return validMediums;
        }
    }
}
